provider "aws" {
  region     = "us-east-1"
  access_key = "AKIA6FH72SYFTNOUYUPN"
  secret_key = "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"
}

variable "env_prefix" {
  description = "environment variable"
  # default = here we can set a default variable
}
variable "vpc_cidr_block" {
    description = "this is the vpc cidr block"
}
resource "aws_vpc" "myapp-vpc"{
  cidr_block = var.vpc_cidr_block
  tags = {
    Name: "${var.env_prefix}-vpc"
    env_dev: "${var.env_prefix}-vpc"
  
  }
}

variable "subnet_cidr_block" {
  description = "this is the cider subnet block"
}

resource "aws_subnet" "myapp-subnet-1" {
  # we should tell terraform to wich vpc this subnet is included
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = "us-east-1a"
  tags = {
    Name : "${var.env_prefix}-subnet-1"
  }
}

resource "aws_internet_gateway" "myapp-igw" {
vpc_id = aws_vpc.myapp-vpc.id
  tags = {
    Name : "${var.env_prefix}-igw"
  }
}

resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_vpc.myapp-vpc.id
  route{
    cidr_block = "0.0.0.0/0" #for the external traffic from the internet to reach our VPC
    gateway_id = aws_internet_gateway.myapp-igw.id
  }
    tags = {
    Name : "${var.env_prefix}-rtb"
  }
}

resource "aws_route_table_association" "a-rtb-subnet-myapp" {
  subnet_id = aws_subnet.myapp-subnet-1.id
  route_table_id = aws_route_table.myapp-route-table.id
}

resource "aws_security_group" "myapp-sg" {
  name = "myapp-sg"
  vpc_id = aws_vpc.myapp-vpc.id
  # now we should set the rules of the firewell , for the incoming traffic (ssh into EC2 , or browser into ec2 , ..) called ingress we define this :
  ingress {
    # we define here the range of ports :
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["197.31.133.255/32"] #here we define the adresses that can access our ressources on port 22 , in our case we have our local machine that will do an ssh session 
  }

   ingress {
    # we define here the range of ports :
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"] #here we define the adresses that can access our ressources on port 8080 , in our case anyone have access from the browser
  }

     egress { #this will allow any traffic to leave the vpc
    # we define here the range of ports , here any port from the vpc to the internet , because we have a usecase of installation like the installation of a docker ilage from the docker hub:
    from_port = 0
    to_port = 0
    protocol = "-1" #any protocol
    cidr_blocks = ["0.0.0.0/0"] #here we define the adresses that can access our ressources on port 8080 , in our case anyone have access from the browser
  }

  tags = {
    Name : "${var.env_prefix}-sg"
  }
}

# data "aws_ami" "latest_amazon_linux_image" {
#   most_recent = true
#   owners = [ "amazon" ]
#   filter {
#     name = "name"
#     values = [ "Amazon Linux 2 AMI (HVM) - Kernel 5.10,*" ]
#   }
#    filter {
#     name = "virtualization-type"
#     values = [ "hvm" ]
#   }
# }

# output "aws_ami_id" {
#   value = data.aws_ami.latest_amazon_linux_image
# }

resource "aws_instance" "myapp-server" {
  ami = "ami-0bb4c991fa89d4b9b"
  instance_type = "t2.micro"
  # only the two defined parameters are mondatory and the other to put this instance in thesubnet defined below 
  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [ aws_security_group.myapp-sg.id ]
  availability_zone = "us-east-1a"
  associate_public_ip_address = true
  key_name = "server-key-pair" #to ssh to the server 

#ici on peut ecrire un script shell ai lieu de l'ecrire apres avoir faire le ssh vers la machine avec le contenu dans user_data : 
# remeber that this will terminate the old ec2 and creta e a new one , also note that this code will be executed only one time.
user_data = file("entry-script.sh")
  
  tags = {
    Name : "${var.env_prefix}-server"
  }
}























# # to query the data from our ressources like the id of a vpc or sth we use data:
# data "aws_vpc" "existing_vpc" {
#   default = true //if we want to retrieve the efault vpc
# }

# # for exemple if we want to reference that data and create a subnet in that vpc :
# resource "aws_subnet" "dev-subnet-2" {
#   # we should tell terraform to wich vpc this subnet is included
#   vpc_id = data.aws_vpc.existing_vpc.id //notice the data.here
#   cidr_block = "172.31.144.0/20" //we should put a cidr that not exist in the default cidr in the ui 
#   availability_zone = "us-east-1a"
#   tags = {
#     Name = "subnet-dev-2"
#   }
# }
# output "vpc-myapp-id" {
#   value = aws_vpc.myapp-vpc.id
# }
